FROM amazoncorretto:11

RUN ln -sf /usr/share/zoneinfo/America/Santiago /etc/localtime \
    && yum -y install glibc-langpack-es

ENV LANG=es_CL.UTF-8
ENV LC_ALL=es_CL.UTF-8

COPY target/*.war ./webapp.war

RUN echo 'yum -y install tzdata-java && \\' >> /run-app.sh \
    && echo 'yum update tzdata tzdata-java && \\' >> /run-app.sh \
    && echo 'java -jar webapp.war -XX:+UseContainerSupport' >> /run-app.sh \
	&& chmod +x /run-app.sh

EXPOSE 8080 7070

CMD /run-app.sh
