insert into perfiles (id_perfil, nombre) values
	(1, 'Administrador'), 
	(2, 'Visita');
alter sequence perfiles_id_perfil_seq RESTART WITH 3;

insert into permisos (id_permiso, codigo, detalle) values
	(1, 'USU-W', 'Permiso de escritura en Módulo Usuarios'),
	(2, 'USU-R', 'Permiso de lectura en Módulo Usuarios'),
	(3, 'PER-W', 'Permiso de escritura en Módulo Personas'),
	(4, 'PER-R', 'Permiso de lectura en Módulo Personas');

insert into perfiles_permisos (perfil_id, permiso_id) values
	(1, 1),
	(1, 2),
	(1, 3),
	(1, 4),
	(2, 2),
	(2, 4);
 