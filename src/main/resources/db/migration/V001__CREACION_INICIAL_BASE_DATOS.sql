create table regiones (
   id_region            int4                 not null,
   nombre               varchar(100)         not null,
   constraint pk_regiones primary key (id_region)
);

create table provincias (
   id_provincia         int4                 not null,
   region_id            int4                 null,
   nombre               varchar(100)         not null,
   constraint pk_provincias primary key (id_provincia),
   constraint fk_provincias foreign key (region_id) references regiones (id_region)
);

create table comunas (
   id_comuna            int4                 not null,
   provincia_id         int4                 null,
   nombre               varchar(100)         not null,
   constraint pk_comunas primary key (id_comuna),
   constraint fk_comunas foreign key (provincia_id) references provincias (id_provincia)
);

create table perfiles (
   id_perfil           serial               not null,
   nombre              varchar(100)         not null,
   constraint pk_perfiles primary key (id_perfil)
);
create unique index idx_perfiles_nombre on perfiles (
	nombre
);

create table permisos (
	id_permiso		int4			not null,
	codigo			varchar(10)		not null,
	detalle			varchar(255)	not null,
	constraint pk_permisos primary key (id_permiso)
);
create unique index idx_permisos_codigo on permisos (
	codigo
);

create table perfiles_permisos (
	perfil_id int4 not null,
	permiso_id int4 not null,
	constraint pk_perfiles_permisos primary key (perfil_id, permiso_id),
	constraint fk_perfiles_permisos_1 foreign key (perfil_id) references perfiles (id_perfil),
	constraint fk_perfiles_permisos_2 foreign key (permiso_id) references permisos (id_permiso)
);

create table personas (
   id_persona           bigserial            not null,
   comuna_id            int4                 null,
   run                  int4                 not null,
   nombres              varchar(255)         not null,
   apellido_paterno      varchar(255)         not null,
   apellido_materno      varchar(255)         null,
   fecha_nacimiento      date                 null,
   telefono             varchar(20)          null,
   constraint pk_personas primary key (id_persona),
   constraint fk_personas foreign key (comuna_id) references comunas (id_comuna)
);

create unique index idx_personas_run on personas (
	run
);

create table usuarios (
   id_usuario           bigserial            not null,
   persona_id           int8                 not null,
   perfil_id            int4                 not null,
   email                varchar(255)         not null,
   password             varchar(500)         null,
   habilitado           boolean              not null,
   constraint pk_usuarios primary key (id_usuario),
   constraint fk_usuarios_1 foreign key (persona_id) references personas (id_persona),
   constraint fk_usuarios_2 foreign key (perfil_id) references perfiles (id_perfil)
);
create unique index idx_usuarios_email on usuarios (
	email
);
