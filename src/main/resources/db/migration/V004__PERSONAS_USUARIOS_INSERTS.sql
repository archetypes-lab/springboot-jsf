insert into personas (id_persona, comuna_id, run, nombres, apellido_paterno, apellido_materno, fecha_nacimiento, telefono) values
	(1, 13132, 11111111, 'Administrador', 'Administrador', 'Administrador', '1990-01-01', '+56 9 12345678'),
	(2, 13132, 22222222, 'Usuario 2', 'Paterno Usuario 2', 'Materno Usuario 2', '1990-01-01', '+56 9 12345678'),
	(3, 13132, 33333333, 'Usuario 3', 'Paterno Usuario 3', 'Materno Usuario 3', '1990-01-01', '+56 9 12345678'),
	(4, 13132, 44444444, 'Usuario 4', 'Paterno Usuario 4', 'Materno Usuario 4', '1990-01-01', '+56 9 12345678'),
	(5, 13132, 55555555, 'Usuario 5', 'Paterno Usuario 5', 'Materno Usuario 5', '1990-01-01', '+56 9 12345678'),
	(6, 13132, 66666666, 'Usuario 6', 'Paterno Usuario 6', 'Materno Usuario 6', '1990-01-01', '+56 9 12345678'),
	(7, 13132, 77777777, 'Usuario 7', 'Paterno Usuario 7', 'Materno Usuario 7', '1990-01-01', '+56 9 12345678'),
	(8, 13132, 88888888, 'Usuario 8', 'Paterno Usuario 8', 'Materno Usuario 8', '1990-01-01', '+56 9 12345678'),
	(9, 13132, 99999999, 'Usuario 9', 'Paterno Usuario 9', 'Materno Usuario 9', '1990-01-01', '+56 9 12345678');

ALTER sequence personas_id_persona_seq RESTART WITH 10;

insert into usuarios (persona_id, perfil_id, email, password, habilitado) values
	(1, 1, 'admin@mailinator.com', '70be2932a9786b17a1351b8d3b9fdf22', true),
	(1, 2, 'admin.consulta@mailinator.com', '70be2932a9786b17a1351b8d3b9fdf22', true),
	(2, 2, 'usuario_1@mailinator.com', '70be2932a9786b17a1351b8d3b9fdf22', true),
	(3, 2, 'usuario_2@mailinator.com', '70be2932a9786b17a1351b8d3b9fdf22', true),
	(4, 2, 'usuario_3@mailinator.com', '70be2932a9786b17a1351b8d3b9fdf22', true),
	(5, 2, 'usuario_5@mailinator.com', '70be2932a9786b17a1351b8d3b9fdf22', true),
	(6, 2, 'usuario_6@mailinator.com', '70be2932a9786b17a1351b8d3b9fdf22', true),
	(7, 2, 'usuario_7@mailinator.com', '70be2932a9786b17a1351b8d3b9fdf22', true),
	(8, 2, 'usuario_8@mailinator.com', '70be2932a9786b17a1351b8d3b9fdf22', true),
	(9, 2, 'usuario_9@mailinator.com', '70be2932a9786b17a1351b8d3b9fdf22', true);
