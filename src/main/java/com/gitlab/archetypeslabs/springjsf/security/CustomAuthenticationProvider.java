package com.gitlab.archetypeslabs.springjsf.security;

import java.util.List;
import java.util.stream.Collectors;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import com.gitlab.archetypeslabs.springjsf.entities.Usuario;
import com.gitlab.archetypeslabs.springjsf.services.PermisoService;
import com.gitlab.archetypeslabs.springjsf.services.UsuarioService;
import com.gitlab.archetypeslabs.springjsf.utils.ConstantesUtil;

@Component
public class CustomAuthenticationProvider implements AuthenticationProvider {

    private static final Logger LOG = LoggerFactory.getLogger(CustomAuthenticationProvider.class);

    @Inject
    private UsuarioService usuarioService;

    @Inject
    private PermisoService permisoService;

    @Inject
    private PasswordEncoder passwordEncoder;

    @Override
    public Authentication authenticate(Authentication auth) {
        var emailEntrada = auth.getName();
        var passEntrada = (String) auth.getCredentials();
        var usuario = usuarioService.traerPorEmail(emailEntrada);
        if (usuario == null) {
            LOG.debug("Usuario no existe!");
            throw new BadCredentialsException(ConstantesUtil.MSJ_ERROR_CREDENCIALES_INVALIDAS);
        }

        if (Boolean.FALSE.equals(usuario.getHabilitado())) {
            LOG.debug("Usuario no habilitado");
            throw new DisabledException(ConstantesUtil.MSJ_ERROR_USUARIO_NO_HABILITADO);
        }

        if (!this.passwordEncoder.matches(passEntrada, usuario.getPassword())) {
            LOG.debug("Password incorrecto!");
            throw new BadCredentialsException(ConstantesUtil.MSJ_ERROR_CREDENCIALES_INVALIDAS);
        }

        return new UsernamePasswordAuthenticationToken(usuario, auth.getCredentials(), getAuthorities(usuario));
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return UsernamePasswordAuthenticationToken.class.isAssignableFrom(authentication);
    }

    private List<GrantedAuthority> getAuthorities(Usuario usuario) {
        if (usuario.getPerfil() == null) {
            throw new DisabledException(ConstantesUtil.MSJ_ERROR_CREDENCIALES_INVALIDAS);
        }
        var permisos = this.permisoService.traerPorPerfil(usuario.getPerfil());
        return permisos.stream().map(permiso -> new SimpleGrantedAuthority(permiso.getCodigo()))
                .collect(Collectors.toList());
    }

}
