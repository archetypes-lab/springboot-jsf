package com.gitlab.archetypeslabs.springjsf.security;

public interface Identidad {

    Long getIdPersona();

    Long getIdUsuario();

    boolean isLoggedIn();

    String getNombrePersona();

    String getEmail();

    boolean tienePermiso(String permiso);

}
