package com.gitlab.archetypeslabs.springjsf.utils;

import java.io.IOException;
import java.util.Optional;
import java.util.function.Function;

import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import org.primefaces.PrimeFaces;

public class JsfUtils {

    private static final String GLOBAL_GROWL_CLIENT_ID = "globalAppGrowl";

    public Optional<String> getRequestParameter(String key) {
        return this.getRequestParameterFunction(key, value -> value);
    }

    public Optional<Integer> getRequestParameterInteger(String key) {
        return this.getRequestParameterFunction(key, Integer::valueOf);
    }

    public Optional<Long> getRequestParameterLong(String key) {
        return this.getRequestParameterFunction(key, Long::valueOf);
    }

    public <T> Optional<T> getRequestParameterFunction(String key, Function<String, T> function) {
        var extContext = getFacesExternalContext();
        var value = extContext.getRequestParameterMap().get(key);
        return value != null ? Optional.of(function.apply(value)) : Optional.empty();
    }

    public void sendRedirect(String relativePath) {
        var context = FacesContext.getCurrentInstance().getExternalContext();
        try {
            context.redirect(context.getRequestContextPath() + relativePath);
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    // mensajes dialogos

    public void showInfoMessageInDialog(String message) {
        this.showMessageInDialog(FacesMessage.SEVERITY_INFO, "Información", message);
    }

    public void showWarnMessageInDialog(String message) {
        this.showMessageInDialog(FacesMessage.SEVERITY_WARN, "Advertencia", message);
    }

    public void showErrorMessageInDialog(String message) {
        this.showMessageInDialog(FacesMessage.SEVERITY_ERROR, "Error", message);
    }

    public void showFatalMessageInDialog(String message) {
        this.showMessageInDialog(FacesMessage.SEVERITY_FATAL, "Error Fatal", message);
    }

    public void showMessageInDialog(Severity severity, String title, String message) {
        PrimeFaces.current().dialog().showMessageDynamic(new FacesMessage(severity, title, message));
    }

    // mensajes growl

    public void showInfoMessageInGrowl(String message) {
        this.showMessageInGrowl(FacesMessage.SEVERITY_INFO, "Información", message);
    }

    public void showWarnMessageInGrowl(String message) {
        this.showMessageInGrowl(FacesMessage.SEVERITY_WARN, "Advertencia", message);
    }

    public void showErrorMessageInGrowl(String message) {
        this.showMessageInGrowl(FacesMessage.SEVERITY_ERROR, "Error", message);
    }

    public void showFatalMessageInGrowl(String message) {
        this.showMessageInGrowl(FacesMessage.SEVERITY_FATAL, "Error Fatal", message);
    }

    public void showMessageInGrowl(Severity severity, String title, String message) {
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(GLOBAL_GROWL_CLIENT_ID, new FacesMessage(severity, title, message));
        context.getExternalContext().getFlash().setKeepMessages(true);
    }

    // privados

    private ExternalContext getFacesExternalContext() {
        return FacesContext.getCurrentInstance().getExternalContext();
    }
}
