package com.gitlab.archetypeslabs.springjsf.entities;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Entity
@Table(name = "regiones")
@Data
@EqualsAndHashCode(callSuper = false, onlyExplicitlyIncluded = true)
public class Region implements Serializable {

	@Id
	@EqualsAndHashCode.Include
	private Integer idRegion;

	@NotBlank
	@Size(max = 100)
	private String nombre;

	@JsonIgnore
	@ToString.Exclude
	@OneToMany(mappedBy = "region", fetch = FetchType.LAZY)
	private Set<Provincia> provincias;
}
