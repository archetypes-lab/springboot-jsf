package com.gitlab.archetypeslabs.springjsf.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Entity
@Table(name = "personas")
@Data
@EqualsAndHashCode(callSuper = false, onlyExplicitlyIncluded = true)
public class Persona implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@EqualsAndHashCode.Include
	private Long idPersona;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "comuna_id")
	private Comuna comuna;

	@NotNull
	@Min(1000000)
	@Max(Integer.MAX_VALUE)
	private Integer run;

	@NotBlank
	@Size(max = 255)
	private String nombres;

	@NotBlank
	@Size(max = 255)
	private String apellidoPaterno;

	@NotBlank
	@Size(max = 255)
	private String apellidoMaterno;

	@Temporal(TemporalType.DATE)
	private Date fechaNacimiento;

	@Size(max = 20)
	private String telefono;

	@JsonIgnore
	@ToString.Exclude
	@OneToMany(mappedBy = "persona", fetch = FetchType.LAZY)
	private Set<Usuario> usuarios;

}
