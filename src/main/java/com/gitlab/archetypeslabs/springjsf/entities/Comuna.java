package com.gitlab.archetypeslabs.springjsf.entities;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Entity
@Table(name = "comunas")
@Data
@EqualsAndHashCode(callSuper = false, onlyExplicitlyIncluded = true)
public class Comuna implements Serializable {

	@Id
	@EqualsAndHashCode.Include
	private Integer idComuna;

	@NotBlank
	@Size(max = 100)
	private String nombre;

	@NotNull
	@JsonIgnore
	@ToString.Exclude
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "provincia_id")
	private Provincia provincia;

	@JsonIgnore
	@ToString.Exclude
	@OneToMany(mappedBy = "comuna", fetch = FetchType.LAZY)
	private Set<Persona> personas;
}
