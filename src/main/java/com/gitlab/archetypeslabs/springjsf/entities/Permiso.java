package com.gitlab.archetypeslabs.springjsf.entities;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Table(name = "permisos")
@Data
@EqualsAndHashCode(callSuper = false, onlyExplicitlyIncluded = true)
public class Permiso implements Serializable {

	@Id
	@EqualsAndHashCode.Include
	private Integer idPermiso;

	@NotBlank
	@Size(max = 10)
	private String codigo;

	@NotBlank
	@Size(max = 255)
	private String detalle;

	@ManyToMany(mappedBy = "permisos", fetch = FetchType.LAZY)
	private Set<Perfil> perfiles;

}
