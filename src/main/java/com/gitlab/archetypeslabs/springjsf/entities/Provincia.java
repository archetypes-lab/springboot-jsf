package com.gitlab.archetypeslabs.springjsf.entities;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Entity
@Table(name = "provincias")
@Data
@EqualsAndHashCode(callSuper = false, onlyExplicitlyIncluded = true)
public class Provincia implements Serializable {

	@Id
	@EqualsAndHashCode.Include
	private Integer idProvincia;

	@NotBlank
	@Size(max = 100)
	private String nombre;

	@NotNull
	@JsonIgnore
	@ToString.Exclude
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "region_id")
	private Region region;

	@JsonIgnore
	@ToString.Exclude
	@OneToMany(mappedBy = "provincia", fetch = FetchType.LAZY)
	private Set<Comuna> comunas;

}
