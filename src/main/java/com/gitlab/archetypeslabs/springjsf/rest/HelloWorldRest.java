package com.gitlab.archetypeslabs.springjsf.rest;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloWorldRest {

    @GetMapping("/hello")
    public String hello() {
        return "Hello world";
    }

}
