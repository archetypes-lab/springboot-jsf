package com.gitlab.archetypeslabs.springjsf.dto;

public enum OperacionCalculadora {
	SUMAR, RESTAR, MULTIPLICAR, DIVIDIR
}
