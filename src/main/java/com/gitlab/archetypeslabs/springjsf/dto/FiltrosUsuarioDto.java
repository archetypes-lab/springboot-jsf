package com.gitlab.archetypeslabs.springjsf.dto;

import java.io.Serializable;

import lombok.Data;

@Data
public class FiltrosUsuarioDto implements Serializable {

    private String email;
    
    private Integer perfilId;
    
    private Long usuarioId;
    
    private Integer runPersona;
    
    private String nombresApellidosPersona;
    
    private Boolean habilitado;
    
}
