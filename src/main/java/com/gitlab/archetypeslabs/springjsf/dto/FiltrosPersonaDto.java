package com.gitlab.archetypeslabs.springjsf.dto;

import java.io.Serializable;
import java.util.Date;

import lombok.Data;

@Data
public class FiltrosPersonaDto implements Serializable {

    private Integer run;
    
    private String nombres;
    
    private String apellidoPaterno;
    
    private String apellidoMaterno;
    
    private Date fechaNacimientoInferior;
    
    private Date fechaNacimientoSuperior;
    
    private Integer regionId;
    
    private Integer provinciaId;
    
    private Integer comunaId;
    
    private String telefono;
    
    private String email;
    
}
