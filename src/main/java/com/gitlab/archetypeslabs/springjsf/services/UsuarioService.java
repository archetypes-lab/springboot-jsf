package com.gitlab.archetypeslabs.springjsf.services;

import java.util.List;

import com.gitlab.archetypeslabs.springjsf.dto.FiltrosUsuarioDto;
import com.gitlab.archetypeslabs.springjsf.entities.Usuario;

public interface UsuarioService {

    Usuario traerPorEmail(String emailEntrada);

    List<Usuario> buscar(FiltrosUsuarioDto filtros);

    void habilitar(Long idUsuario);

    void deshabilitar(Long idUsuario);

    void cambiarPassword(Long idUsuario, String passsword);

    Long guardar(Usuario usuario);

    void eliminar(Usuario usuario);

}