package com.gitlab.archetypeslabs.springjsf.services.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.inject.Inject;

import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.gitlab.archetypeslabs.springjsf.dto.FiltrosPersonaDto;
import com.gitlab.archetypeslabs.springjsf.entities.Persona;
import com.gitlab.archetypeslabs.springjsf.exceptions.LogicaNegocioException;
import com.gitlab.archetypeslabs.springjsf.repository.PersonaRepository;
import com.gitlab.archetypeslabs.springjsf.repository.queriesdsl.PersonaQueryDsl;
import com.gitlab.archetypeslabs.springjsf.services.PersonaService;

@Service
@Scope(proxyMode = ScopedProxyMode.INTERFACES)
@Transactional(readOnly = true)
public class PersonaServiceImpl implements PersonaService {

	@Inject
	private PersonaRepository personaRepository;

	@Inject
	private PersonaQueryDsl personaQueryDsl;

	@Override
	public long buscarRowCount(FiltrosPersonaDto filtrosDto) {
		return this.personaQueryDsl.buscarRowCount(filtrosDto);
	}

	@Override
	public List<Persona> buscar(FiltrosPersonaDto filtros) {
		return this.personaQueryDsl.buscar(filtros, 0, Long.MAX_VALUE);
	}

	@Override
	public List<Persona> buscar(FiltrosPersonaDto filtrosDto, long offset, long limit) {
		return this.personaQueryDsl.buscar(filtrosDto, offset, limit);
	}

	@Transactional(readOnly = false)
	@Override
	public Long guardar(Persona persona) {
		if (persona == null) {
			throw new IllegalArgumentException("Entrada persona es nulo");
		}

		var optional = persona.getIdPersona() != null ? this.personaRepository.findById(persona.getIdPersona())
				: Optional.<Persona>empty();
		var personaExistente = optional.isPresent() ? optional.get() : null;

		var erroresValidaciones = new ArrayList<String>();
		erroresValidaciones.addAll(validarRunExistente(persona, personaExistente));
		if (!erroresValidaciones.isEmpty()) {
			throw new LogicaNegocioException(erroresValidaciones);
		}
		var personaAttached = this.personaRepository.save(persona);
		return personaAttached.getIdPersona();
	}

	@Transactional(readOnly = false)
	@Override
	public void eliminar(Persona persona) {
		var personaAttached = this.traerPersonaAttached(persona.getIdPersona());
		var erroresValidaciones = new ArrayList<String>();
		erroresValidaciones.addAll(validarPersonaSinUsuarios(personaAttached));
		if (!erroresValidaciones.isEmpty()) {
			throw new LogicaNegocioException(erroresValidaciones);
		}
		this.personaRepository.delete(personaAttached);
	}

	@Override
	public Persona traerPorRun(Integer run) {
		var optional = this.personaRepository.findByRun(run);
		return optional.isPresent() ? optional.get() : null;
	}

	@Override
	public Persona traerPorId(Long idPersona) {
		return traerPersonaAttached(idPersona);
	}

	@Override
	public Persona traerPorIdConFetchs(Long idPersona) {
		return this.personaQueryDsl.traerPorIdConFetchs(idPersona);
	}

	// private

	private Persona traerPersonaAttached(Long idPersona) {
		var optional = this.personaRepository.findById(idPersona);
		if (!optional.isPresent()) {
			throw new IllegalArgumentException(String.format("No existe la persona con id: %d", idPersona));
		}
		return optional.get();
	}

	private List<String> validarPersonaSinUsuarios(Persona persona) {
		var errores = new ArrayList<String>();
		var usuarios = persona.getUsuarios();
		if (usuarios != null && !usuarios.isEmpty()) {
			errores.add("No se puede eliminar una persona que tiene usuarios. Primero elimine sus usuarios.");
		}
		return errores;
	}

	private List<String> validarRunExistente(Persona personaTarget, Persona personaExistente) {
		var errores = new ArrayList<String>();
		var personaRutOptional = this.personaRepository.findByRun(personaTarget.getRun());
		if (!personaRutOptional.isPresent() || personaRutOptional.get().equals(personaExistente)) {
			return errores;
		}
		errores.add(String.format("No se encuentra disponible el run: %d", personaTarget.getRun()));
		return errores;
	}

}
