package com.gitlab.archetypeslabs.springjsf.services;

import java.util.List;

import com.gitlab.archetypeslabs.springjsf.entities.Comuna;

public interface ComunaService {

    List<Comuna> traerPorIdProvincia(Integer idProvincia);

    Comuna traerPorId(Integer idComuna);

}
