package com.gitlab.archetypeslabs.springjsf.services.impl;

import java.util.List;

import javax.inject.Inject;

import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.gitlab.archetypeslabs.springjsf.entities.Perfil;
import com.gitlab.archetypeslabs.springjsf.repository.PerfilRepository;
import com.gitlab.archetypeslabs.springjsf.services.PerfilService;

@Service
@Scope(proxyMode = ScopedProxyMode.INTERFACES)
@Transactional(readOnly = true)
public class PerfilServiceImpl implements PerfilService {

    @Inject
    private PerfilRepository perfilRepository;

    @Override
    public List<Perfil> traerPerfiles() {
        return this.perfilRepository.findByOrderByNombreDesc();
    }

    @Override
    public Perfil traerPorId(Integer idPerfil) {
        var optional = this.perfilRepository.findById(idPerfil);
        return optional.isPresent() ? optional.get() : null;
    }

}
