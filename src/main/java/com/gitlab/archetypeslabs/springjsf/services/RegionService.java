package com.gitlab.archetypeslabs.springjsf.services;

import java.util.List;

import com.gitlab.archetypeslabs.springjsf.entities.Region;

public interface RegionService {

    List<Region> traerRegiones();

    Region traerPorId(Integer idRegion);

}
