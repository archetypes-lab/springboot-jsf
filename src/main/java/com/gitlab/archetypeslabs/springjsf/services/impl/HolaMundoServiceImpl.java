package com.gitlab.archetypeslabs.springjsf.services.impl;

import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Service;

import com.gitlab.archetypeslabs.springjsf.services.HolaMundoService;

@Service
@Scope(proxyMode = ScopedProxyMode.INTERFACES)
public class HolaMundoServiceImpl implements HolaMundoService {

	@Override
	public String saludar() {
		return "Hola Mundo desde el Servicio";
	}

}
