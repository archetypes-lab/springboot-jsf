package com.gitlab.archetypeslabs.springjsf.services;

import java.util.List;

import com.gitlab.archetypeslabs.springjsf.entities.Provincia;

public interface ProvinciaService {

    List<Provincia> traerPorIdRegion(Integer idRegion);

    Provincia traerPorId(Integer idProvincia);

}
