package com.gitlab.archetypeslabs.springjsf.services.impl;

import java.util.List;

import javax.inject.Inject;

import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.gitlab.archetypeslabs.springjsf.entities.Region;
import com.gitlab.archetypeslabs.springjsf.repository.RegionRepository;
import com.gitlab.archetypeslabs.springjsf.services.RegionService;

@Service
@Scope(proxyMode = ScopedProxyMode.INTERFACES)
@Transactional(readOnly = true)
public class RegionServiceImpl implements RegionService {

    @Inject
    private RegionRepository regionRepository;

    @Override
    public List<Region> traerRegiones() {
        return this.regionRepository.findByOrderByNombreAsc();
    }

    @Override
    public Region traerPorId(Integer idRegion) {
        var optional = this.regionRepository.findById(idRegion);
        return optional.isPresent() ? optional.get() : null;
    }

}
