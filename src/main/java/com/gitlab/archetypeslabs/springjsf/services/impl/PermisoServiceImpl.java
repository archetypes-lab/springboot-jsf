package com.gitlab.archetypeslabs.springjsf.services.impl;

import java.util.List;

import javax.inject.Inject;

import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.gitlab.archetypeslabs.springjsf.entities.Perfil;
import com.gitlab.archetypeslabs.springjsf.entities.Permiso;
import com.gitlab.archetypeslabs.springjsf.repository.PermisoRepository;
import com.gitlab.archetypeslabs.springjsf.services.PermisoService;

@Service
@Scope(proxyMode = ScopedProxyMode.INTERFACES)
@Transactional(readOnly = true)
public class PermisoServiceImpl implements PermisoService {

    @Inject
    private PermisoRepository permisoRepository;

    @Override
    public List<Permiso> traerPorPerfil(Perfil perfil) {
        return this.permisoRepository.findByPerfilesContains(perfil);
    }

    @Override
    public Permiso traerPorId(Integer idPermiso) {
        var optional = this.permisoRepository.findById(idPermiso);
        return optional.isPresent() ? optional.get() : null;
    }

}
