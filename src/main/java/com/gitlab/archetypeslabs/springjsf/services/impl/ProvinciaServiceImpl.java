package com.gitlab.archetypeslabs.springjsf.services.impl;

import java.util.List;

import javax.inject.Inject;

import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.gitlab.archetypeslabs.springjsf.entities.Provincia;
import com.gitlab.archetypeslabs.springjsf.entities.Region;
import com.gitlab.archetypeslabs.springjsf.repository.ProvinciaRepository;
import com.gitlab.archetypeslabs.springjsf.services.ProvinciaService;

@Service
@Scope(proxyMode = ScopedProxyMode.INTERFACES)
@Transactional(readOnly = true)
public class ProvinciaServiceImpl implements ProvinciaService {

    @Inject
    private ProvinciaRepository provinciaRepository;

    @Override
    public List<Provincia> traerPorIdRegion(Integer idRegion) {
        var region = new Region();
        region.setIdRegion(idRegion);
        return this.provinciaRepository.findByRegionOrderByNombreDesc(region);
    }

    @Override
    public Provincia traerPorId(Integer idProvincia) {
        var optional = this.provinciaRepository.findById(idProvincia);
        return optional.isPresent() ? optional.get() : null;
    }

}
