package com.gitlab.archetypeslabs.springjsf.services.impl;

import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Service;

import com.gitlab.archetypeslabs.springjsf.dto.OperacionCalculadora;
import com.gitlab.archetypeslabs.springjsf.exceptions.LogicaNegocioException;
import com.gitlab.archetypeslabs.springjsf.services.CalculadoraService;

@Service
@Scope(proxyMode = ScopedProxyMode.INTERFACES)
public class CalculadoraServiceImpl implements CalculadoraService {

	@Override
	public Float operar(Float operador1, Float operador2, OperacionCalculadora operacion) {

		if (operador1 == null) {
			throw new LogicaNegocioException("Operador1 es nulo");
		}

		if (operador2 == null) {
			throw new LogicaNegocioException("Operador2 es nulo");
		}

		switch (operacion) {
			case SUMAR:
				return operador1 + operador2;
			case RESTAR:
				return operador1 - operador2;
			case MULTIPLICAR:
				return operador1 * operador2;
			case DIVIDIR:
				return operador1 / operador2;
			default:
				throw new IllegalArgumentException("Operacion no es conocida");
		}

	}

}
