package com.gitlab.archetypeslabs.springjsf.services.impl;

import java.util.List;

import javax.inject.Inject;

import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.gitlab.archetypeslabs.springjsf.entities.Comuna;
import com.gitlab.archetypeslabs.springjsf.entities.Provincia;
import com.gitlab.archetypeslabs.springjsf.repository.ComunaRepository;
import com.gitlab.archetypeslabs.springjsf.services.ComunaService;

@Service
@Scope(proxyMode = ScopedProxyMode.INTERFACES)
@Transactional(readOnly = true)
public class ComunaServiceImpl implements ComunaService {

    @Inject
    private ComunaRepository comunaRepository;

    @Override
    public List<Comuna> traerPorIdProvincia(Integer idProvincia) {
        var provincia = new Provincia();
        provincia.setIdProvincia(idProvincia);
        return this.comunaRepository.findByProvinciaOrderByNombreDesc(provincia);
    }

    @Override
    public Comuna traerPorId(Integer idComuna) {
        var optional = this.comunaRepository.findById(idComuna);
        return optional.isPresent() ? optional.get() : null;
    }

}
