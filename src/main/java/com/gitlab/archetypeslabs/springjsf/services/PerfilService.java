package com.gitlab.archetypeslabs.springjsf.services;

import java.util.List;

import com.gitlab.archetypeslabs.springjsf.entities.Perfil;

public interface PerfilService {

    List<Perfil> traerPerfiles();

    Perfil traerPorId(Integer idPerfil);

}
