package com.gitlab.archetypeslabs.springjsf.services;

import java.util.List;

import com.gitlab.archetypeslabs.springjsf.dto.FiltrosPersonaDto;
import com.gitlab.archetypeslabs.springjsf.entities.Persona;

public interface PersonaService {

    long buscarRowCount(FiltrosPersonaDto filtrosDto);

    List<Persona> buscar(FiltrosPersonaDto filtros);

    List<Persona> buscar(FiltrosPersonaDto filtrosDto, long offset, long limit);

    Long guardar(Persona persona);

    void eliminar(Persona persona);

    Persona traerPorRun(Integer run);

    Persona traerPorId(Long idPersona);

    Persona traerPorIdConFetchs(Long idPersona);

}
