package com.gitlab.archetypeslabs.springjsf.services;

import com.gitlab.archetypeslabs.springjsf.dto.OperacionCalculadora;

public interface CalculadoraService {

	Float operar(Float operador1, Float operador2, OperacionCalculadora operacion);
}
