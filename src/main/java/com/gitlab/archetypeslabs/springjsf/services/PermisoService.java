package com.gitlab.archetypeslabs.springjsf.services;

import java.util.List;

import com.gitlab.archetypeslabs.springjsf.entities.Perfil;
import com.gitlab.archetypeslabs.springjsf.entities.Permiso;

public interface PermisoService {

    List<Permiso> traerPorPerfil(Perfil perfil);

    Permiso traerPorId(Integer idPermiso);
}
