package com.gitlab.archetypeslabs.springjsf.services.impl;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.gitlab.archetypeslabs.springjsf.dto.FiltrosUsuarioDto;
import com.gitlab.archetypeslabs.springjsf.entities.Usuario;
import com.gitlab.archetypeslabs.springjsf.exceptions.LogicaNegocioException;
import com.gitlab.archetypeslabs.springjsf.repository.UsuarioRepository;
import com.gitlab.archetypeslabs.springjsf.repository.queriesdsl.UsuarioQueryDsl;
import com.gitlab.archetypeslabs.springjsf.services.UsuarioService;

@Service
@Scope(proxyMode = ScopedProxyMode.INTERFACES)
@Transactional(readOnly = true)
public class UsuarioServiceImpl implements UsuarioService {

	@Inject
	private UsuarioRepository usuarioRepository;

	@Inject
	private UsuarioQueryDsl usuarioQueryDsl;

	@Inject
	private PasswordEncoder passwordEncoder;

	@Override
	public Usuario traerPorEmail(String email) {
		var optional = this.usuarioRepository.findByEmail(email);
		return optional.isPresent() ? optional.get() : null;
	}

	@Override
	public List<Usuario> buscar(FiltrosUsuarioDto filtros) {
		return this.usuarioQueryDsl.buscar(filtros);
	}

	@Transactional(readOnly = false)
	@Override
	public void habilitar(Long idUsuario) {
		this.habilitar(idUsuario, true);
	}

	@Transactional(readOnly = false)
	@Override
	public void deshabilitar(Long idUsuario) {
		this.habilitar(idUsuario, false);
	}

	@Transactional(readOnly = false)
	@Override
	public void cambiarPassword(Long idUsuario, String passsword) {
		var usuario = this.trarUsuarioAttached(idUsuario);
		var passEncoded = this.passwordEncoder.encode(passsword);
		usuario.setPassword(passEncoded);
	}

	@Transactional(readOnly = false)
	@Override
	public Long guardar(Usuario usuario) {
		if (usuario == null) {
			throw new IllegalArgumentException("usuario es nulo");
		}

		var optional = this.usuarioRepository.findById(usuario.getIdUsuario());
		var usuarioExistente = optional.isPresent() ? optional.get() : null;

		// validaciones
		List<String> erroresValidaciones = new ArrayList<>();
		erroresValidaciones.addAll(validarEmailUnico(usuario, usuarioExistente));
		if (!erroresValidaciones.isEmpty()) {
			throw new LogicaNegocioException(erroresValidaciones);
		}

		// caso solo crear
		if (usuario.getIdUsuario() == null) {
			usuario.setHabilitado(true);
		}

		// persistencia
		var usuarioSaved = this.usuarioRepository.save(usuario);
		return usuarioSaved.getIdUsuario();
	}

	@Transactional(readOnly = false)
	@Override
	public void eliminar(Usuario usuario) {
		if (usuario == null || usuario.getIdUsuario() == null) {
			return;
		}
		this.usuarioRepository.deleteById(usuario.getIdUsuario());
	}

	// privados

	private void habilitar(Long idUsuario, boolean habilitar) {
		var usuario = trarUsuarioAttached(idUsuario);
		usuario.setHabilitado(habilitar);
	}

	private Usuario trarUsuarioAttached(Long idUsuario) {
		var optional = this.usuarioRepository.findById(idUsuario);
		if (!optional.isPresent()) {
			throw new IllegalStateException("No existe el usuario: " + idUsuario);
		}
		return optional.get();
	}

	private List<String> validarEmailUnico(Usuario usuario, Usuario usuarioExistente) {
		var errores = new ArrayList<String>();
		if (usuarioExistente == null || usuarioExistente.equals(usuario)) {
			return errores;
		}

		var idUsuario = usuario != null ? usuario.getIdUsuario() : null;
		var idUsuarioExistente = usuarioExistente.getIdUsuario();

		var mail = usuario != null ? usuario.getEmail() : null;
		var mailExistente = usuarioExistente.getEmail();

		if (!idUsuarioExistente.equals(idUsuario) && mailExistente.equals(mail)) {
			errores.add(String.format("no se encuentra disponible el correo '%s'", usuario.getEmail()));
		}
		return errores;
	}

}
