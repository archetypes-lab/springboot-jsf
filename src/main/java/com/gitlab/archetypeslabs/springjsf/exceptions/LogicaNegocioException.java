package com.gitlab.archetypeslabs.springjsf.exceptions;

import java.util.List;

public class LogicaNegocioException extends RuntimeException {

	private final List<String> errores;

	public LogicaNegocioException(String message) {
		super(message);
		this.errores = List.of(message);
	}

	public LogicaNegocioException(List<String> errors) {
		super(toHtmlUnorderedList(errors));
		this.errores = errors;
	}

	private static String toHtmlUnorderedList(List<String> errors) {
		var builder = new StringBuilder("<ul>");
		for (var error : errors) {
			builder.append(String.format("<li>%s</li>", error));
		}
		builder.append("</ul>");
		return builder.toString();
	}

	public List<String> getErrores() {
		return errores;
	}
}
