package com.gitlab.archetypeslabs.springjsf.config;

import java.util.EnumSet;

import javax.servlet.DispatcherType;

import org.primefaces.webapp.filter.FileUploadFilter;
import org.springframework.boot.autoconfigure.web.servlet.DispatcherServletAutoConfiguration;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.tuckey.web.filters.urlrewrite.UrlRewriteFilter;

import com.gitlab.archetypeslabs.springjsf.filters.ViewExpiredFilter;

@Configuration
public class MvcConfig {

	@Bean
	public CommonsMultipartResolver filterMultipartResolver() {
		CommonsMultipartResolver filterMultipartResolver = new CommonsMultipartResolver();
		filterMultipartResolver.setMaxUploadSize(20971520);
		return filterMultipartResolver;
	}

	/*
	 * Filter URL REWRITE
	 */

	@Bean
	public FilterRegistrationBean<UrlRewriteFilter> urlRewriteFilter() {
		var registration = new FilterRegistrationBean<UrlRewriteFilter>();
		registration.setFilter(new UrlRewriteFilter());
		registration.addUrlPatterns("/*");
		registration.setDispatcherTypes(EnumSet.of(DispatcherType.FORWARD, DispatcherType.REQUEST,
				DispatcherType.INCLUDE, DispatcherType.ASYNC, DispatcherType.ERROR));
		registration.setName("urlRewriteFilter");
		registration.setOrder(1);
		return registration;
	}

	@Bean
	public FilterRegistrationBean<ViewExpiredFilter> viewExpiredFilter() {
		var registration = new FilterRegistrationBean<ViewExpiredFilter>();
		registration.setFilter(new ViewExpiredFilter("/sesionExpirada.xhtml"));
		registration.addUrlPatterns("/*");
		registration.setDispatcherTypes(EnumSet.of(DispatcherType.FORWARD, DispatcherType.REQUEST,
				DispatcherType.INCLUDE, DispatcherType.ASYNC, DispatcherType.ERROR));
		registration.setName("viewExpiredFilter");
		registration.setOrder(2);
		return registration;
	}

	@Bean
	public FilterRegistrationBean<FileUploadFilter> primefacesFileuploadFilter() {
		var registration = new FilterRegistrationBean<FileUploadFilter>();
		registration.setFilter(new FileUploadFilter());
		registration
				.addServletNames(DispatcherServletAutoConfiguration.DEFAULT_DISPATCHER_SERVLET_REGISTRATION_BEAN_NAME);
		registration.setDispatcherTypes(EnumSet.of(DispatcherType.FORWARD, DispatcherType.REQUEST,
				DispatcherType.INCLUDE, DispatcherType.ASYNC, DispatcherType.ERROR));
		registration.setName("primefacesFileuploadFilter");
		registration.setOrder(3);
		return registration;
	}

}
