package com.gitlab.archetypeslabs.springjsf.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.hibernate5.Hibernate5Module;

@Configuration
public class JacksonConfig implements WebMvcConfigurer {

	@Primary
	@Bean(name = "objectMapper")
	public ObjectMapper hibernateAwareObjectMapper() {
		var hibernate5module = new Hibernate5Module();
		hibernate5module.enable(Hibernate5Module.Feature.SERIALIZE_IDENTIFIER_FOR_LAZY_NOT_LOADED_OBJECTS);
		ObjectMapper mapper = new ObjectMapper();
		mapper.registerModule(hibernate5module);
		return mapper;
	}

}
