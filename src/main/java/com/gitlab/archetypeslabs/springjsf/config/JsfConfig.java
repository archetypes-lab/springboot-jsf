package com.gitlab.archetypeslabs.springjsf.config;

import javax.faces.webapp.FacesServlet;
import javax.servlet.MultipartConfigElement;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.web.servlet.ServletContextInitializer;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.web.context.WebApplicationContext;

import com.gitlab.archetypeslabs.springjsf.jsf.converters.EntityConverter;
import com.gitlab.archetypeslabs.springjsf.jsf.converters.RutConverter;
import com.gitlab.archetypeslabs.springjsf.jsf.converters.jsonconverter.JsonConverter;
import com.gitlab.archetypeslabs.springjsf.utils.JsfUtils;
import com.gitlab.archetypeslabs.springjsf.utils.RutConverterUtils;

@Configuration
public class JsfConfig {

	private static final Logger log = LoggerFactory.getLogger(JsfConfig.class);

	@Bean
	public ServletRegistrationBean<FacesServlet> servletRegistrationBean() {
		var servlet = new FacesServlet();
		var servletRegistrationBean = new ServletRegistrationBean<FacesServlet>(servlet, "*.xhtml");
		servletRegistrationBean.setMultipartConfig(new MultipartConfigElement(null, 500000000, 500000000, 1048576));
		return servletRegistrationBean;
	}

	@SuppressWarnings("squid:S1191")
	@Configuration
	static class CommonsConfigureJSFContextParameters implements ServletContextInitializer {
		@Override
		public void onStartup(ServletContext servletContext) throws ServletException {

			log.info("==> Iniciando configuración común JSF");

			servletContext.addListener(com.sun.faces.config.ConfigureListener.class);

			servletContext.setInitParameter("com.sun.faces.forceLoadConfiguration", Boolean.TRUE.toString());
			servletContext.setInitParameter("org.jboss.jbossfaces.WAR_BUNDLES_JSF_IMPL", Boolean.TRUE.toString());

			servletContext.setInitParameter("javax.faces.DATETIMECONVERTER_DEFAULT_TIMEZONE_IS_SYSTEM_TIMEZONE",
					Boolean.TRUE.toString());
			servletContext.setInitParameter("javax.faces.DEFAULT_SUFFIX", ".xhtml");
			servletContext.setInitParameter("javax.faces.STATE_SAVING_METHOD", "server");
			servletContext.setInitParameter("javax.faces.PARTIAL_STATE_SAVING", Boolean.TRUE.toString());
			servletContext.setInitParameter("javax.faces.FACELETS_SKIP_COMMENTS", Boolean.TRUE.toString());
			servletContext.setInitParameter("javax.faces.FACELETS_LIBRARIES", "/WEB-INF/primefaces-omega.taglib.xml");
			servletContext.setInitParameter("javax.faces.INTERPRET_EMPTY_STRING_SUBMITTED_VALUES_AS_NULL",
					Boolean.TRUE.toString());

			servletContext.setInitParameter("primefaces.THEME", "nova-light");
			servletContext.setInitParameter("primefaces.UPLOADER", "commons");
			servletContext.setInitParameter("primefaces.FONT_AWESOME", Boolean.FALSE.toString());
			servletContext.setInitParameter("primefaces.TRANSFORM_METADATA", Boolean.TRUE.toString());

			servletContext.setInitParameter("org.apache.myfaces.PRETTY_HTML", Boolean.TRUE.toString());
			servletContext.setInitParameter("org.apache.myfaces.ALLOW_JAVASCRIPT", Boolean.TRUE.toString());
			servletContext.setInitParameter("org.apache.myfaces.READONLY_AS_DISABLED_FOR_SELECTS",
					Boolean.TRUE.toString());
			servletContext.setInitParameter("org.apache.myfaces.RENDER_VIEWSTATE_ID", Boolean.TRUE.toString());
			servletContext.setInitParameter("org.apache.myfaces.STRICT_XHTML_LINKS", Boolean.TRUE.toString());
			servletContext.setInitParameter("org.apache.myfaces.CONFIG_REFRESH_PERIOD", "2");
			servletContext.setInitParameter("org.apache.myfaces.DETECT_JAVASCRIPT", Boolean.FALSE.toString());
			servletContext.setInitParameter("org.apache.myfaces.AUTO_SCROLL", Boolean.FALSE.toString());
			servletContext.setInitParameter("org.apache.myfaces.ADD_RESOURCE_CLASS",
					"org.apache.myfaces.renderkit.html.util.DefaultAddResource");
			servletContext.setInitParameter("org.apache.myfaces.RESOURCE_VIRTUAL_PATH",
					"/faces/myFacesExtensionResource");
			servletContext.setInitParameter("org.apache.myfaces.CHECK_EXTENSIONS_FILTER", Boolean.TRUE.toString());
		}
	}

	@Configuration
	@ConditionalOnProperty(value = "app.jsf.production", havingValue = "false", matchIfMissing = true)
	static class DevelopmentConfigureJSFContextParameters implements ServletContextInitializer {
		@Override
		public void onStartup(ServletContext servletContext) throws ServletException {

			log.info("==> Iniciando configuración especifica de desarrollo JSF");

			servletContext.setInitParameter("javax.faces.PROJECT_STAGE", "Development");
			servletContext.setInitParameter("javax.faces.FACELETS_REFRESH_PERIOD", "1");
			servletContext.setInitParameter("facelets.DEVELOPMENT", "true");
		}
	}

	@Configuration
	@ConditionalOnProperty(value = "app.jsf.production", havingValue = "true", matchIfMissing = false)
	static class ProductionConfigureJSFContextParameters implements ServletContextInitializer {
		@Override
		public void onStartup(ServletContext servletContext) throws ServletException {

			log.info("==> Iniciando configuración especifica productiva JSF");

			servletContext.setInitParameter("javax.faces.PROJECT_STAGE", "Production");
			servletContext.setInitParameter("javax.faces.FACELETS_REFRESH_PERIOD", "-1");
			servletContext.setInitParameter("facelets.DEVELOPMENT", "false");
		}
	}

	@Bean
	@Scope(value = WebApplicationContext.SCOPE_APPLICATION, proxyMode = ScopedProxyMode.TARGET_CLASS)
	public RutConverter rutConverter() {
		return new RutConverter(new RutConverterUtils());
	}

	@Bean
	@Scope(value = WebApplicationContext.SCOPE_APPLICATION, proxyMode = ScopedProxyMode.TARGET_CLASS)
	public EntityConverter entityConverter() {
		return new EntityConverter();
	}

	@Bean
	@Scope(value = WebApplicationContext.SCOPE_APPLICATION, proxyMode = ScopedProxyMode.TARGET_CLASS)
	public JsfUtils jsfUtils() {
		return new JsfUtils();
	}

	@Bean
	@Scope(value = WebApplicationContext.SCOPE_APPLICATION, proxyMode = ScopedProxyMode.TARGET_CLASS)
	public JsonConverter jsonConverter() {
		return new JsonConverter();
	}

}