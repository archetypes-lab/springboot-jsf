package com.gitlab.archetypeslabs.springjsf.config;

import java.util.Set;

import javax.faces.application.ViewExpiredException;

import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;
import org.springframework.boot.web.server.ErrorPage;
import org.springframework.boot.web.server.WebServerFactoryCustomizer;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;

@Configuration
public class TomcatConfig implements WebServerFactoryCustomizer<TomcatServletWebServerFactory> {

	@Override
	public void customize(TomcatServletWebServerFactory factory) {

		factory.setErrorPages(Set.of(new ErrorPage(ViewExpiredException.class, "/sesionExpirada.xhtml"),
				new ErrorPage(HttpStatus.NOT_FOUND, "/404.xhtml"),
				new ErrorPage(HttpStatus.UNAUTHORIZED, "/access.xhtml"),
				new ErrorPage(HttpStatus.INTERNAL_SERVER_ERROR, "/error.xhtml")));
	}
}
