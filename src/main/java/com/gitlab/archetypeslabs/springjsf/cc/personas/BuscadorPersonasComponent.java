package com.gitlab.archetypeslabs.springjsf.cc.personas;

import java.io.Serializable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gitlab.archetypeslabs.springjsf.dto.FiltrosPersonaDto;
import com.gitlab.archetypeslabs.springjsf.managedbeans.personas.PersonaFiltrosComp;
import com.gitlab.archetypeslabs.springjsf.managedbeans.personas.PersonaLazyDataModel;

import lombok.Data;

@Data
public class BuscadorPersonasComponent implements Serializable {

    private static final Logger log = LoggerFactory.getLogger(BuscadorPersonasComponent.class);
    
    private PersonaFiltrosComp filtrosComp;
    
    private PersonaLazyDataModel personaLazyDataModel;
    
    public void init() {
	filtrosComp = new PersonaFiltrosComp();
	filtrosComp.setEventoCambiaronFiltros(this::cambioDeFiltrosListener);
	this.cambioDeFiltrosListener(new FiltrosPersonaDto());
    }
    
    public void cambioDeFiltrosListener(FiltrosPersonaDto filtrosDto) {
	log.debug("Filtros Cambiados: {}", filtrosDto);
	personaLazyDataModel = new PersonaLazyDataModel(filtrosDto);
    }
    
}
