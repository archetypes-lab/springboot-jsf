package com.gitlab.archetypeslabs.springjsf.cc.filtros;

import java.util.List;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import javax.faces.model.SelectItem;

import com.gitlab.archetypeslabs.springjsf.services.ComunaService;
import com.gitlab.archetypeslabs.springjsf.services.ProvinciaService;
import com.gitlab.archetypeslabs.springjsf.services.RegionService;
import com.gitlab.archetypeslabs.springjsf.utils.SpringAppContext;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
public class FiltroRegProComSeamless extends FiltroRegProCom {

    public FiltroRegProComSeamless() {
	// vacio
    }
    
    public FiltroRegProComSeamless(
	    String idFiltro, 
	    String etiqueta, 
	    FiltroRegProComData data) {
	super(idFiltro, 
		etiqueta, 
		data);
    }
    
    @Override
    public Supplier<List<SelectItem>> getRegionesSupplier() {
	return ()-> SpringAppContext.getBeanFromContext(RegionService.class)
		.traerRegiones().stream()
		.map(region -> new SelectItem(region.getIdRegion(), region.getNombre()))
		.collect(Collectors.toList());
    }

    @Override
    public Function<Integer, List<SelectItem>> getProvinciasFunction() {
	return idRegion -> SpringAppContext.getBeanFromContext(ProvinciaService.class)
		.traerPorIdRegion(idRegion).stream()
		.map(prov -> new SelectItem(prov.getIdProvincia(), prov.getNombre()))
		.collect(Collectors.toList());
    }

    @Override
    public Function<Integer, List<SelectItem>> getComunasFunction() {
	return idProv -> SpringAppContext.getBeanFromContext(ComunaService.class)
		.traerPorIdProvincia(idProv).stream()
		.map(com -> new SelectItem(com.getIdComuna(), com.getNombre()))
		.collect(Collectors.toList());
    }
    
    
}
