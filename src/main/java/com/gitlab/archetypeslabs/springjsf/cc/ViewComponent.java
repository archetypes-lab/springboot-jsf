package com.gitlab.archetypeslabs.springjsf.cc;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnore;

public interface ViewComponent extends Serializable {

    @JsonIgnore
    String getVistaXhtml();
    
}
