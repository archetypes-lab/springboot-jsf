package com.gitlab.archetypeslabs.springjsf.cc.filtros;

public class FiltroRut extends Filtro<Integer> {

    private static final String VISTA_XHTML = "/filtros/rut.xhtml";
    
    public FiltroRut() {
	// vacio
    }
    
    public FiltroRut(String id, String etiqueta) {
	super(id, etiqueta);
    }
    
    @Override
    public String getVistaXhtml() {
	return VISTA_XHTML;
    }
    
}
