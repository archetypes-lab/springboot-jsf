package com.gitlab.archetypeslabs.springjsf.cc.filtros;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import com.gitlab.archetypeslabs.springjsf.cc.filtros.Filtro.Estado;

import lombok.Data;

@Data
public abstract class AbstractFiltros<T extends Serializable> implements Serializable {

    private EventoAgregarFiltro eventoAgregarFiltro;
    
    private EventoCambiaronFiltros<T> eventoCambiaronFiltros;
    
    private Filtro<? extends Serializable> filtroDisponible;
    
    private List<Filtro<? extends Serializable>> filtrosDiponibles; 
    
    private List<Filtro<? extends Serializable>> filtrosVisibles;
    
    
    public AbstractFiltros() {
	this.filtrosVisibles = new ArrayList<>();
	this.filtrosDiponibles = new ArrayList<>();
    }
    
    public AbstractFiltros(List<Filtro<? extends Serializable>> filtros) {
	this.filtrosDiponibles = new ArrayList<>(filtros);
	this.filtrosVisibles = new ArrayList<>();
    }
    
    public void seleccionarFiltroAction() { 
	this.filtroDisponible.setEstado(Estado.CONFIGURACION);
	this.filtrosDiponibles.remove(this.filtroDisponible);
	this.filtrosVisibles.add(this.filtroDisponible);
	if(this.eventoAgregarFiltro != null) {
	    this.eventoAgregarFiltro.postAgregado(this.filtroDisponible, this.filtrosDiponibles, this.filtrosVisibles);
	}
	this.filtroDisponible = null;
    }
    
    public void aplicar(Filtro<Serializable> filtro) {
	filtro.setEstado(Estado.APLICADO);
	if(this.eventoCambiaronFiltros != null) {
	    this.eventoCambiaronFiltros.aplicar(convertToDto(getFiltrosAplicados()));
	}
    }
    
    public void quitar(Filtro<Serializable> filtro) {
	var index = this.filtrosVisibles.indexOf(filtro);
	this.filtrosVisibles.remove(index);
	this.filtrosDiponibles.add(filtro);
	filtro.limpiar();
	if(this.eventoCambiaronFiltros != null) {
	    this.eventoCambiaronFiltros.aplicar(convertToDto(getFiltrosAplicados()));
	}
    }
    
    public void configurar(Filtro<Serializable> filtro) {
	filtro.setEstado(Estado.CONFIGURACION);
    }
    
    public void deshabilitar(Filtro<Serializable> filtro) {
	filtro.setEstado(Estado.DESHABILITADO);
	if(this.eventoCambiaronFiltros != null) {
	    this.eventoCambiaronFiltros.aplicar(convertToDto(getFiltrosAplicados()));
	}
    }
    
    public abstract T convertToDto(List<Filtro<? extends Serializable>> filtrosAplicados);
    
    private List<Filtro<? extends Serializable>> getFiltrosAplicados() {
	if(this.filtrosVisibles == null) {
	    return Collections.emptyList();
	}
	return this.filtrosVisibles.stream()
		.filter(filtro -> Estado.APLICADO.equals(filtro.getEstado()))
		.collect(Collectors.toList());
    }

}
