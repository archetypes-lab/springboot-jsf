package com.gitlab.archetypeslabs.springjsf.cc.filtros;

import java.io.Serializable;
import java.util.List;

public interface EventoAgregarFiltro extends Serializable {

    void postAgregado(Filtro<? extends Serializable> agregado, List<Filtro<? extends Serializable>> disponibles, List<Filtro<? extends Serializable>> visibles);

}
