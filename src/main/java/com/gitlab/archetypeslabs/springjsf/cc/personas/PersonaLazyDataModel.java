package com.gitlab.archetypeslabs.springjsf.cc.personas;

import org.primefaces.model.LazyDataModel;

import com.gitlab.archetypeslabs.springjsf.entities.Persona;

public class PersonaLazyDataModel extends LazyDataModel<Persona>{

}
