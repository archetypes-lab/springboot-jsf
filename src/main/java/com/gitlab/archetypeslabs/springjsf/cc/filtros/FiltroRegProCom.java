package com.gitlab.archetypeslabs.springjsf.cc.filtros;

import java.io.Serializable;
import java.util.List;
import java.util.function.Function;
import java.util.function.Supplier;

import javax.faces.model.SelectItem;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
public abstract class FiltroRegProCom extends Filtro<FiltroRegProCom.FiltroRegProComData> {

    private static final String VISTA_XHTML = "/filtros/regProCom.xhtml";
    
    private FiltroRegProComData data;
    
    private List<SelectItem> regiones;
    
    private List<SelectItem> provincias;
    
    private List<SelectItem> comunas;
    
    public FiltroRegProCom() {
	// vacio
    }
    
    @JsonIgnore
    public abstract Supplier<List<SelectItem>> getRegionesSupplier();
	
    @JsonIgnore
    public abstract Function<Integer, List<SelectItem>> getProvinciasFunction();
    
    @JsonIgnore
    public abstract Function<Integer, List<SelectItem>> getComunasFunction();
    
    public FiltroRegProCom(
	    String idFiltro, 
	    String etiqueta, 
	    FiltroRegProComData data) {
	super(idFiltro, etiqueta);
	this.data = data != null ? data : new FiltroRegProComData();
	cargarRegiones();
	cargarProvincias();
	cargarComunas();
    }

    public void cambioDeRegionListener() {
	this.data.setProvinciaId(null);
	this.provincias = null;
	this.data.setComunaId(null);
	this.comunas = null;
	cargarProvincias();
    }
    
    public void cambioDeProvinciaListener() {
	this.data.setComunaId(null);
	this.comunas = null;
	cargarComunas();
    }
    
    @Override
    public void limpiar() {
	super.limpiar();
	this.provincias = null;
	this.comunas = null;
	this.data.setRegionId(null);
	this.data.setProvinciaId(null);
	this.data.setComunaId(null);
    }
    
    @Override
    public String getVistaXhtml() {
	return VISTA_XHTML;
    }
    
    // privados
    
    private void cargarRegiones() {
	this.regiones = this.getRegionesSupplier().get();
    }
    
    private void cargarProvincias() {
	if(this.data.regionId != null) {
	    this.provincias = this.getProvinciasFunction().apply(this.data.regionId);
	}
    }
    
    private void cargarComunas() {
	if(this.data.provinciaId != null) {
	    this.comunas = this.getComunasFunction().apply(this.data.provinciaId);
	}
    }

    // clases estaticas

    @Data
    public static class FiltroRegProComData implements Serializable {
	
	private Integer regionId;
	
	private Integer provinciaId;
	
	private Integer comunaId;
	
    }
    
}
