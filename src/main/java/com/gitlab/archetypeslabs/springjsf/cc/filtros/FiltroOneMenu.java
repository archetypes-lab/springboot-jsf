package com.gitlab.archetypeslabs.springjsf.cc.filtros;

import java.io.Serializable;
import java.util.List;

import javax.faces.model.SelectItem;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Data
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
public class FiltroOneMenu<T extends Serializable> extends Filtro<T>{

    private static final String VISTA_XHTML = "/filtros/oneMenu.xhtml";
    
    @Getter
    @Setter
    private List<SelectItem> selectsItems;
    
    public FiltroOneMenu() {
	// vacio
    }
    
    public FiltroOneMenu(String id, String etiqueta, List<SelectItem> selectsItems) {
	super(id, etiqueta);
	this.selectsItems = selectsItems;
    }
    
    @Override
    public String getVistaXhtml() {
	return VISTA_XHTML;
    }

}
