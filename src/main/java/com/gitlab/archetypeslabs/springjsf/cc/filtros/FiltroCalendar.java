package com.gitlab.archetypeslabs.springjsf.cc.filtros;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
public class FiltroCalendar<T extends Date> extends Filtro<T> {

    private static final String VISTA_XHTML = "/filtros/calendar.xhtml";

    private Opciones opciones;
    
    public FiltroCalendar() {
	this.opciones = new Opciones();
    }
    
    public FiltroCalendar(String id, String etiqueta) {
	super(id, etiqueta);
	this.opciones = new Opciones();
    }
    
    public FiltroCalendar(String id, String etiqueta, Opciones opciones) {
	super(id, etiqueta);
	this.opciones = opciones;
    }
    
    @Override
    public String getVistaXhtml() {
	return VISTA_XHTML;
    }
    
    @Data
    public static class Opciones implements Serializable {
	
	private static final int DEFAULT_ANIOS_SUMADOS = 10;
	    
	private static final int DEFAULT_ANIOS_RESTADOS = 20;
	    
	private static final boolean DEFAULT_NAVEGADOR_ACTIVADO = true;
	
	private static final String DEFAULT_PATTERN = "dd/MM/yyyy";
	
	private static final String DEFAULT_LANG = "es";
	
	private int aniosSumados;
	
	private int aniosRestados;
	
	private boolean navegadorActivado;
	
	private String pattern;
	
	private String lang;
	
	@JsonIgnore
	public String getYearRange() {
	    return String.format("c-%d:c+%d", aniosRestados, aniosSumados);
	}
	
	public Opciones() {
	    this.aniosSumados = DEFAULT_ANIOS_SUMADOS;
	    this.aniosRestados = DEFAULT_ANIOS_RESTADOS;
	    this.navegadorActivado = DEFAULT_NAVEGADOR_ACTIVADO;
	    this.pattern = DEFAULT_PATTERN;
	    this.lang = DEFAULT_LANG;
	}
	
    }
    
}
