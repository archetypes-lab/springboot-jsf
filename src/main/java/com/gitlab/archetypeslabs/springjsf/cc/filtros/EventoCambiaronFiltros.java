package com.gitlab.archetypeslabs.springjsf.cc.filtros;

import java.io.Serializable;

public interface EventoCambiaronFiltros<T extends Serializable> extends Serializable {

    void aplicar(T data);
    
}
