package com.gitlab.archetypeslabs.springjsf.cc.filtros;

import java.io.Serializable;

import com.gitlab.archetypeslabs.springjsf.cc.ViewComponent;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false, onlyExplicitlyIncluded = true)
public class Filtro<T extends Serializable> implements ViewComponent {
    
    private static final String VISTA_XHTML = "/filtros/basico.xhtml";

    public enum Estado { CONFIGURACION, APLICADO, DESHABILITADO }

    @EqualsAndHashCode.Include
    private String id;
    
    private String etiqueta;
    
    private String textoDeAyuda;
    
    private Estado estado;
    
    private T valor;
    
    public Filtro() {
	// empty
	this.textoDeAyuda = "";
	this.estado = Estado.CONFIGURACION;
    }
    
    public Filtro(String id, String etiqueta) {
	this();
	this.id = id;
	this.etiqueta = etiqueta;
    }

    public void limpiar() {
	this.valor = null;
    }

    @Override
    public String getVistaXhtml() {
	return VISTA_XHTML;
    }
    
}
