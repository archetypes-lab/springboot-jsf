package com.gitlab.archetypeslabs.springjsf.jsf.converters;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;

import com.gitlab.archetypeslabs.springjsf.utils.RutConverterUtils;
import com.gitlab.archetypeslabs.springjsf.utils.RutConverterUtils.RutConverterException;

public class RutConverter implements Converter<Integer> {

	private RutConverterUtils rutConverterUtils;

	public RutConverter(RutConverterUtils rutConverterUtils) {
		this.rutConverterUtils = rutConverterUtils;
	}

	public Integer getAsObject(FacesContext context, UIComponent component, String value) {
		try {
			return this.rutConverterUtils.asInteger(value);
		} catch (RutConverterException ex) {
			FacesMessage message = new FacesMessage();
			message.setDetail(ex.getMessage());
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			throw new ConverterException(message);
		}
	}

	public String getAsString(FacesContext context, UIComponent component, Integer rut) {
		try {
			return this.rutConverterUtils.asString(rut);
		} catch (RutConverterException ex) {
			FacesMessage message = new FacesMessage();
			message.setDetail(ex.getMessage());
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			throw new ConverterException(message);
		}
	}
}