package com.gitlab.archetypeslabs.springjsf.jsf.converters.jsonconverter;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;

import org.apache.commons.codec.binary.Base64;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class JsonConverter implements Converter<Object> {

	private static final String SEPARADOR_JSON = "_JSON_";

	private static final String ERROR_DE_CONVERSION_MSG = "Error de conversión";

	public Object getAsObject(FacesContext fc, UIComponent uic, String objetSerializedParam) {
		if (objetSerializedParam == null || objetSerializedParam.trim().isEmpty()) {
			return null;
		}
		Pattern p = Pattern.compile("^(.*)" + SEPARADOR_JSON + "(.*)");

		String objetSerialized;
		try {
			objetSerialized = new String(Base64.decodeBase64(objetSerializedParam));
		} catch (IllegalArgumentException ex) {
			objetSerialized = objetSerializedParam;
		} catch (RuntimeException ex) {
			throw new ConverterException(ERROR_DE_CONVERSION_MSG, ex);
		}

		Matcher m = p.matcher(objetSerialized);
		if (!m.matches()) {
			return null;
		}
		String className = m.group(1);
		String json = m.group(2);
		Class<?> clazz;
		try {
			clazz = Class.forName(className);
		} catch (ClassNotFoundException ex) {
			throw new ConverterException(ERROR_DE_CONVERSION_MSG);
		}

		return jsonToObject(json, clazz);

	}

	public String getAsString(FacesContext fc, UIComponent uic, Object o) {
		if (o == null) {
			return null;
		}
		String json = objectToJson(o);
		String clazz = o.getClass().getName();
		try {
			return Base64.encodeBase64String((clazz + SEPARADOR_JSON + json).getBytes());
		} catch (RuntimeException ex) {
			throw new ConverterException(ERROR_DE_CONVERSION_MSG, ex);
		}
	}

	private static String objectToJson(Object o) {
		try {
			return new ObjectMapper().writeValueAsString(o);
		} catch (JsonProcessingException e) {
			throw new IllegalStateException("Error de conversión objeto a json", e);
		}
	}

	private Object jsonToObject(String json, Class<?> clazz) {
		try {
			return new ObjectMapper().readValue(json, clazz);
		} catch (JsonProcessingException e) {
			throw new IllegalStateException("Error de conversión json a objeto", e);
		}
	}

}