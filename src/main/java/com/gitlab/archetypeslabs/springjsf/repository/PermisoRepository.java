package com.gitlab.archetypeslabs.springjsf.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.gitlab.archetypeslabs.springjsf.entities.Perfil;
import com.gitlab.archetypeslabs.springjsf.entities.Permiso;

@Repository
public interface PermisoRepository extends JpaRepository<Permiso, Integer> {

    List<Permiso> findByPerfilesContains(Perfil perfil);

}
