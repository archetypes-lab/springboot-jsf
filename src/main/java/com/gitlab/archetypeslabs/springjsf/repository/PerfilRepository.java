package com.gitlab.archetypeslabs.springjsf.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.gitlab.archetypeslabs.springjsf.entities.Perfil;

@Repository
public interface PerfilRepository extends JpaRepository<Perfil, Integer> {

    List<Perfil> findByOrderByNombreDesc();

}
