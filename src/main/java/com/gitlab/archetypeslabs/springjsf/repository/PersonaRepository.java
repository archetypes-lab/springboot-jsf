package com.gitlab.archetypeslabs.springjsf.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.gitlab.archetypeslabs.springjsf.entities.Persona;

@Repository
public interface PersonaRepository extends JpaRepository<Persona, Long> {

	Optional<Persona> findByRun(Integer run);

}
