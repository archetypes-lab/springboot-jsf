package com.gitlab.archetypeslabs.springjsf.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.gitlab.archetypeslabs.springjsf.entities.Provincia;
import com.gitlab.archetypeslabs.springjsf.entities.Region;

@Repository
public interface ProvinciaRepository extends JpaRepository<Provincia, Integer> {

    List<Provincia> findByRegionOrderByNombreDesc(Region region);

}
