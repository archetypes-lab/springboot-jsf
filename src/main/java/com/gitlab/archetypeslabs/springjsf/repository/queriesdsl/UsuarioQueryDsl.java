package com.gitlab.archetypeslabs.springjsf.repository.queriesdsl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import com.gitlab.archetypeslabs.springjsf.dto.FiltrosUsuarioDto;
import com.gitlab.archetypeslabs.springjsf.entities.QPerfil;
import com.gitlab.archetypeslabs.springjsf.entities.QPersona;
import com.gitlab.archetypeslabs.springjsf.entities.QUsuario;
import com.gitlab.archetypeslabs.springjsf.entities.Usuario;
import com.querydsl.core.types.ExpressionUtils;
import com.querydsl.core.types.Predicate;
import com.querydsl.jpa.impl.JPAQuery;

@Repository
public class UsuarioQueryDsl {

	@PersistenceContext
	private EntityManager em;

	public List<Usuario> buscar(FiltrosUsuarioDto filtros) {
		var qUsuario = QUsuario.usuario;
		var qPersona = QPersona.persona;
		var qPerfil = QPerfil.perfil;
		return new JPAQuery<Usuario>(em).from(qUsuario).distinct().leftJoin(qUsuario.persona, qPersona)
				.leftJoin(qUsuario.perfil, qPerfil)
				.where(ExpressionUtils.allOf(filtrarPorEmail(filtros, qUsuario), filtrarPorUsuarioId(filtros, qUsuario),
						filtrarPorPerfilId(filtros, qPerfil), filtrarPorRunPersona(filtros, qPersona),
						filtrarPorNombresApellidosPersona(filtros, qPersona), filtrarPorHabilitado(filtros, qUsuario)))
				.orderBy(qUsuario.email.desc()).fetch();
	}

	private Predicate filtrarPorUsuarioId(FiltrosUsuarioDto filtros, QUsuario qUsuario) {
		if (filtros == null || filtros.getUsuarioId() == null) {
			return null;
		}
		return qUsuario.idUsuario.eq(filtros.getUsuarioId());
	}

	private Predicate filtrarPorEmail(FiltrosUsuarioDto filtros, QUsuario qUsuario) {
		if (filtros == null || filtros.getEmail() == null) {
			return null;
		}
		return qUsuario.email.likeIgnoreCase("%" + filtros.getEmail() + "%");
	}

	private Predicate filtrarPorPerfilId(FiltrosUsuarioDto filtros, QPerfil qPerfil) {
		if (filtros == null || filtros.getPerfilId() == null) {
			return null;
		}
		return qPerfil.idPerfil.eq(filtros.getPerfilId());
	}

	private Predicate filtrarPorRunPersona(FiltrosUsuarioDto filtros, QPersona qPersona) {
		if (filtros == null || filtros.getRunPersona() == null) {
			return null;
		}
		return qPersona.run.eq(filtros.getRunPersona());
	}

	private Predicate filtrarPorNombresApellidosPersona(FiltrosUsuarioDto filtros, QPersona qPersona) {
		if (filtros == null || filtros.getNombresApellidosPersona() != null) {
			return null;
		}
		var likeMatchString = "%" + filtros.getNombresApellidosPersona() + "%";
		return ExpressionUtils.anyOf(qPersona.nombres.likeIgnoreCase(likeMatchString),
				qPersona.apellidoPaterno.likeIgnoreCase(likeMatchString),
				qPersona.apellidoMaterno.likeIgnoreCase(likeMatchString));
	}

	private Predicate filtrarPorHabilitado(FiltrosUsuarioDto filtros, QUsuario qUsuario) {
		if (filtros == null || filtros.getHabilitado() != null) {
			return null;
		}
		return qUsuario.habilitado.eq(filtros.getHabilitado());
	}

}
