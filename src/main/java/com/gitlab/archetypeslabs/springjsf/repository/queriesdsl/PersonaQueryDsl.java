package com.gitlab.archetypeslabs.springjsf.repository.queriesdsl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import com.gitlab.archetypeslabs.springjsf.dto.FiltrosPersonaDto;
import com.gitlab.archetypeslabs.springjsf.entities.Persona;
import com.gitlab.archetypeslabs.springjsf.entities.QComuna;
import com.gitlab.archetypeslabs.springjsf.entities.QPersona;
import com.gitlab.archetypeslabs.springjsf.entities.QProvincia;
import com.gitlab.archetypeslabs.springjsf.entities.QRegion;
import com.gitlab.archetypeslabs.springjsf.entities.QUsuario;
import com.querydsl.core.types.ExpressionUtils;
import com.querydsl.core.types.Predicate;
import com.querydsl.jpa.impl.JPAQuery;

@Repository
public class PersonaQueryDsl {

	@PersistenceContext
	private EntityManager em;

	public Persona traerPorIdConFetchs(Long idPersona) {
		var qPersona = QPersona.persona;
		var qComuna = QComuna.comuna;
		var qProvincia = QProvincia.provincia;
		var qRegion = QRegion.region;
		return new JPAQuery<Persona>(em).select(qPersona).distinct().from(qPersona).leftJoin(qPersona.comuna, qComuna)
				.fetchJoin().leftJoin(qComuna.provincia, qProvincia).fetchJoin().leftJoin(qProvincia.region, qRegion)
				.fetchJoin().where(qPersona.idPersona.eq(idPersona)).fetchOne();
	}

	public long buscarRowCount(FiltrosPersonaDto filtros) {
		var qPersona = QPersona.persona;
		var qUsuario = QUsuario.usuario;
		var qRegion = QRegion.region;
		var qProvincia = QProvincia.provincia;
		var qComuna = QComuna.comuna;
		return new JPAQuery<Persona>(em).select(qPersona).distinct().from(qPersona)
				.leftJoin(qPersona.usuarios, qUsuario).leftJoin(qPersona.comuna, qComuna)
				.leftJoin(qComuna.provincia, qProvincia).leftJoin(qProvincia.region, qRegion)
				.where(getPredicados(filtros, qPersona, qUsuario, qComuna, qProvincia, qRegion)).fetchCount();
	}

	public List<Persona> buscar(FiltrosPersonaDto filtros, long offset, long limit) {
		var qPersona = QPersona.persona;
		var qUsuario = QUsuario.usuario;
		var qRegion = QRegion.region;
		var qProvincia = QProvincia.provincia;
		var qComuna = QComuna.comuna;
		return new JPAQuery<Persona>(em).select(qPersona).distinct().from(qPersona)
				.leftJoin(qPersona.usuarios, qUsuario).fetchJoin().leftJoin(qPersona.comuna, qComuna).fetchJoin()
				.leftJoin(qComuna.provincia, qProvincia).fetchJoin().leftJoin(qProvincia.region, qRegion).fetchJoin()
				.where(getPredicados(filtros, qPersona, qUsuario, qComuna, qProvincia, qRegion))
				.orderBy(qPersona.nombres.desc()).offset(offset).limit(limit).fetch();
	}

	private static Predicate getPredicados(FiltrosPersonaDto filtros, QPersona qPersona, QUsuario qUsuario,
			QComuna qComuna, QProvincia qProvincia, QRegion qRegion) {
		return ExpressionUtils.allOf(getRunPredicate(filtros, qPersona), getNombresPredicate(filtros, qPersona),
				getApellidoPaternoPredicate(filtros, qPersona), getApellidoMaternoPredicate(filtros, qPersona),
				getFechaNacimientoInferiorPredicate(filtros, qPersona),
				getFechaNacimientoSuperiorPredicate(filtros, qPersona), getRegionPredicate(filtros, qRegion),
				getProvinciaPredicate(filtros, qProvincia), getComunaPredicate(filtros, qComuna),
				getEmailPredicate(filtros, qUsuario));
	}

	private static Predicate getRunPredicate(FiltrosPersonaDto filtros, QPersona qPersona) {
		if (filtros == null || filtros.getRun() == null) {
			return null;
		}
		return qPersona.run.eq(filtros.getRun());
	}

	private static Predicate getNombresPredicate(FiltrosPersonaDto filtros, QPersona qPersona) {
		if (filtros == null || filtros.getNombres() == null || filtros.getNombres().isBlank()) {
			return null;
		}
		var likeMatchString = "%" + filtros.getNombres() + "%";
		return qPersona.nombres.likeIgnoreCase(likeMatchString);
	}

	private static Predicate getApellidoPaternoPredicate(FiltrosPersonaDto filtros, QPersona qPersona) {
		if (filtros == null || filtros.getApellidoPaterno() == null || filtros.getApellidoPaterno().isBlank()) {
			return null;
		}
		var likeMatchString = "%" + filtros.getApellidoPaterno() + "%";
		return qPersona.apellidoPaterno.likeIgnoreCase(likeMatchString);
	}

	private static Predicate getApellidoMaternoPredicate(FiltrosPersonaDto filtros, QPersona qPersona) {
		if (filtros == null || filtros.getApellidoMaterno() == null || filtros.getApellidoMaterno().isBlank()) {
			return null;
		}
		var likeMatchString = "%" + filtros.getApellidoMaterno() + "%";
		return qPersona.apellidoMaterno.likeIgnoreCase(likeMatchString);
	}

	private static Predicate getFechaNacimientoInferiorPredicate(FiltrosPersonaDto filtros, QPersona qPersona) {
		if (filtros == null || filtros.getFechaNacimientoInferior() == null) {
			return null;
		}
		return qPersona.fechaNacimiento.before(filtros.getFechaNacimientoInferior());
	}

	private static Predicate getFechaNacimientoSuperiorPredicate(FiltrosPersonaDto filtros, QPersona qPersona) {
		if (filtros == null || filtros.getFechaNacimientoSuperior() == null) {
			return null;
		}
		return qPersona.fechaNacimiento.after(filtros.getFechaNacimientoSuperior());
	}

	private static Predicate getEmailPredicate(FiltrosPersonaDto filtros, QUsuario qUsuario) {
		if (filtros == null || filtros.getEmail() == null || filtros.getEmail().isBlank()) {
			return null;
		}
		return qUsuario.email.eq(filtros.getEmail());
	}

	private static Predicate getComunaPredicate(FiltrosPersonaDto filtros, QComuna qComuna) {
		if (filtros == null || filtros.getComunaId() == null) {
			return null;
		}
		return qComuna.idComuna.eq(filtros.getComunaId());
	}

	private static Predicate getProvinciaPredicate(FiltrosPersonaDto filtros, QProvincia qProvincia) {
		if (filtros == null || filtros.getProvinciaId() == null) {
			return null;
		}
		return qProvincia.idProvincia.eq(filtros.getProvinciaId());
	}

	private static Predicate getRegionPredicate(FiltrosPersonaDto filtros, QRegion qRegion) {
		if (filtros == null || filtros.getRegionId() == null) {
			return null;
		}
		return qRegion.idRegion.eq(filtros.getRegionId());
	}

}
