package com.gitlab.archetypeslabs.springjsf.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.gitlab.archetypeslabs.springjsf.entities.Comuna;
import com.gitlab.archetypeslabs.springjsf.entities.Provincia;

@Repository
public interface ComunaRepository extends JpaRepository<Comuna, Integer> {

    List<Comuna> findByProvinciaOrderByNombreDesc(Provincia provincia);

}
