package com.gitlab.archetypeslabs.springjsf.managedbeans;

import javax.faces.context.FacesContext;

import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.RequestScope;

@Component
@RequestScope
public class MessagesBean {

    public boolean existenMensajesGlobales() {
        FacesContext fc = FacesContext.getCurrentInstance();
        return (fc.getMessages(null) != null) && (fc.getMessages(null).hasNext());
    }

}
