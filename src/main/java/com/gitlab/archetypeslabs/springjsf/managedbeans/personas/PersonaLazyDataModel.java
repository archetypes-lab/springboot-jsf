package com.gitlab.archetypeslabs.springjsf.managedbeans.personas;

import java.util.List;
import java.util.Map;

import org.primefaces.model.FilterMeta;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.gitlab.archetypeslabs.springjsf.dto.FiltrosPersonaDto;
import com.gitlab.archetypeslabs.springjsf.entities.Persona;
import com.gitlab.archetypeslabs.springjsf.services.PersonaService;
import com.gitlab.archetypeslabs.springjsf.utils.SpringAppContext;

import lombok.Getter;
import lombok.Setter;

public class PersonaLazyDataModel extends LazyDataModel<Persona> {

    @Getter
    @Setter
    private FiltrosPersonaDto filtrosDto;

    public PersonaLazyDataModel() {
        // empty
    }

    public PersonaLazyDataModel(FiltrosPersonaDto filtrosDto) {
        this.filtrosDto = filtrosDto;
        this.setRowCount((int) this.getPersonaService().buscarRowCount(this.filtrosDto));
    }

    @Override
    public List<Persona> load(int first, int pageSize, String sortField, SortOrder sortOrder,
            Map<String, FilterMeta> filterBy) {
        return this.getPersonaService().buscar(this.filtrosDto, first, pageSize);
    }

    @JsonIgnore
    public PersonaService getPersonaService() {
        return SpringAppContext.getBeanFromContext(PersonaService.class);
    }

}
