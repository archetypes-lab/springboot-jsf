package com.gitlab.archetypeslabs.springjsf.managedbeans;

import java.io.Serializable;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.RequestScope;

import com.gitlab.archetypeslabs.springjsf.services.HolaMundoService;

import lombok.Getter;
import lombok.Setter;

@Component
@RequestScope
public class InicioBean implements Serializable {

    private static final Logger log = LoggerFactory.getLogger(InicioBean.class);

    @Autowired
    private transient HolaMundoService holaMundoService;

    @Getter
    @Setter
    private String saludo;

    @PostConstruct
    public void iniciar() {
        saludo = this.holaMundoService.saludar();
        log.debug("El saludo es: {}", saludo);
    }

}
