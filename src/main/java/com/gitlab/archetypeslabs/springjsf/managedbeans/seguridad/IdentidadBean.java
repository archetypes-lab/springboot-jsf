package com.gitlab.archetypeslabs.springjsf.managedbeans.seguridad;

import java.io.Serializable;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.RequestScope;

import com.gitlab.archetypeslabs.springjsf.entities.Persona;
import com.gitlab.archetypeslabs.springjsf.entities.Usuario;
import com.gitlab.archetypeslabs.springjsf.security.Identidad;

@Component("identidad")
@RequestScope
public class IdentidadBean implements Identidad, Serializable {

	@Override
	public boolean tienePermiso(String permiso) {
		UsernamePasswordAuthenticationToken token = getSecurityToken();
		return token != null && token.getAuthorities().stream()
				.anyMatch(grantedAuthority -> grantedAuthority.getAuthority().equals(permiso));
	}

	@Override
	public Long getIdPersona() {
		var persona = getPersona();
		return persona != null ? persona.getIdPersona() : null;
	}

	@Override
	public boolean isLoggedIn() {
		return getUsuarioDesdeSesion() != null;
	}

	@Override
	public String getNombrePersona() {
		var persona = getPersona();
		return persona != null ? persona.getNombres() : null;
	}

	@Override
	public Long getIdUsuario() {
		var usuario = getUsuarioDesdeSesion();
		return usuario != null ? usuario.getIdUsuario() : null;
	}

	@Override
	public String getEmail() {
		var usuario = getUsuarioDesdeSesion();
		return usuario != null ? usuario.getEmail() : null;
	}

	// privados

	private Persona getPersona() {
		var usuario = getUsuarioDesdeSesion();
		return usuario != null ? usuario.getPersona() : null;
	}

	private UsernamePasswordAuthenticationToken getSecurityToken() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if (!(authentication instanceof UsernamePasswordAuthenticationToken)) {
			return null;
		}
		return (UsernamePasswordAuthenticationToken) authentication;
	}

	private Usuario getUsuarioDesdeSesion() {
		UsernamePasswordAuthenticationToken token = getSecurityToken();
		if (token != null && token.getPrincipal() != null && (token.getPrincipal() instanceof Usuario)) {
			return (Usuario) token.getPrincipal();
		}
		return null;
	}

}
