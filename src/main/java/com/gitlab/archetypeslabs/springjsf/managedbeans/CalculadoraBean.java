package com.gitlab.archetypeslabs.springjsf.managedbeans;

import java.io.Serializable;

import javax.inject.Inject;

import com.gitlab.archetypeslabs.springjsf.dto.OperacionCalculadora;
import com.gitlab.archetypeslabs.springjsf.services.CalculadoraService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.RequestScope;

import lombok.Getter;
import lombok.Setter;

@Component
@RequestScope
public class CalculadoraBean implements Serializable {

	private static final Logger log = LoggerFactory.getLogger(CalculadoraBean.class);

	@Inject
	private CalculadoraService calculadoraService;

	@Getter
	@Setter
	private Float operador1;

	@Getter
	@Setter
	private Float operador2;

	@Getter
	@Setter
	private Float memoria;

	public void onLoad() {
		log.info("===> OnLoad: Pagina Calculadora");
		this.operador1 = 0f;
		this.operador2 = 0f;
		this.memoria = null;
	}

	public void sumar() {
		this.memoria = this.calculadoraService.operar(this.operador1, this.operador2, OperacionCalculadora.SUMAR);
	}

	public void restar() {
		this.memoria = this.calculadoraService.operar(this.operador1, this.operador2, OperacionCalculadora.RESTAR);
	}

	public void multiplicar() {
		this.memoria = this.calculadoraService.operar(this.operador1, this.operador2, OperacionCalculadora.MULTIPLICAR);
	}

	public void dividir() {
		this.memoria = this.calculadoraService.operar(this.operador1, this.operador2, OperacionCalculadora.DIVIDIR);
	}

	public void usarMemoria() {
		this.operador1 = this.memoria;
		this.memoria = null;
		this.operador2 = null;
	}

}
