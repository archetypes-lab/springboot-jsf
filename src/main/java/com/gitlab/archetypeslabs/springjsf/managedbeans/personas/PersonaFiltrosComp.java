package com.gitlab.archetypeslabs.springjsf.managedbeans.personas;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gitlab.archetypeslabs.springjsf.cc.filtros.AbstractFiltros;
import com.gitlab.archetypeslabs.springjsf.cc.filtros.Filtro;
import com.gitlab.archetypeslabs.springjsf.cc.filtros.FiltroCalendar;
import com.gitlab.archetypeslabs.springjsf.cc.filtros.FiltroRegProCom;
import com.gitlab.archetypeslabs.springjsf.cc.filtros.FiltroRegProComSeamless;
import com.gitlab.archetypeslabs.springjsf.cc.filtros.FiltroRut;
import com.gitlab.archetypeslabs.springjsf.dto.FiltrosPersonaDto;

public class PersonaFiltrosComp extends AbstractFiltros<FiltrosPersonaDto> {

	private static final Logger log = LoggerFactory.getLogger(PersonaFiltrosComp.class);

	public PersonaFiltrosComp() {
		super(crearFiltrosPersona());
		this.setEventoCambiaronFiltros(filtros -> log.debug("filtros aplicados: {}", filtros));
	}

	private static List<Filtro<? extends Serializable>> crearFiltrosPersona() {
		var opcionesCalendario = new FiltroCalendar.Opciones();
		opcionesCalendario.setAniosRestados(80);
		opcionesCalendario.setAniosSumados(0);
		return List.of(new FiltroRut("rut", "RUT"), new Filtro<>("nombres", "Nombres"),
				new Filtro<>("apPaterno", "Apellido Paterno"), new Filtro<>("apMaterno", "Apellido Materno"),
				new FiltroCalendar<>("fNacInferior", "Fecha Nacimiento Inferior A", opcionesCalendario),
				new FiltroCalendar<>("fNacSuperior", "Fecha Nacimiento Superior A", opcionesCalendario),
				new Filtro<>("email", "Correo Electronico"),
				new FiltroRegProComSeamless("territorio", "Territorio", null));
	}

	@Override
	public FiltrosPersonaDto convertToDto(List<Filtro<? extends Serializable>> filtrosAplicados) {
		var filtros = new FiltrosPersonaDto();
		for (var aplicado : filtrosAplicados) {
			switch (aplicado.getId()) {
				case "nombres":
					filtros.setNombres(((String) aplicado.getValor()));
					break;
				case "apPaterno":
					filtros.setApellidoPaterno((String) aplicado.getValor());
					break;
				case "email":
					filtros.setEmail((String) aplicado.getValor());
					break;
				case "rut":
					filtros.setRun((Integer) aplicado.getValor());
					break;
				case "territorio":
					var territorio = ((FiltroRegProCom) aplicado).getData();
					filtros.setRegionId(territorio.getRegionId());
					filtros.setProvinciaId(territorio.getProvinciaId());
					filtros.setComunaId(territorio.getComunaId());
					break;
				case "fNacInferior":
					var fechaInf = (Date) aplicado.getValor();
					filtros.setFechaNacimientoInferior(fechaInf);
					break;
				case "fNacSuperior":
					var fechaSup = (Date) aplicado.getValor();
					filtros.setFechaNacimientoSuperior(fechaSup);
					break;
				default:
					log.warn("no se reconoce el id de filtro: {}", aplicado.getId());
			}
		}
		return filtros;
	}

}
