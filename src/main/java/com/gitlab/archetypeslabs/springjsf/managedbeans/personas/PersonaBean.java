package com.gitlab.archetypeslabs.springjsf.managedbeans.personas;

import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;

import javax.faces.model.SelectItem;
import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.RequestScope;

import com.gitlab.archetypeslabs.springjsf.entities.Comuna;
import com.gitlab.archetypeslabs.springjsf.entities.Persona;
import com.gitlab.archetypeslabs.springjsf.services.ComunaService;
import com.gitlab.archetypeslabs.springjsf.services.PersonaService;
import com.gitlab.archetypeslabs.springjsf.services.ProvinciaService;
import com.gitlab.archetypeslabs.springjsf.services.RegionService;
import com.gitlab.archetypeslabs.springjsf.utils.JsfUtils;

import lombok.Getter;
import lombok.Setter;

@Component
@RequestScope
public class PersonaBean implements Serializable {

    private static final Logger log = LoggerFactory.getLogger(PersonaBean.class);

    @Inject
    private PersonaService personaService;

    @Inject
    private RegionService regionService;

    @Inject
    private ProvinciaService provinciaService;

    @Inject
    private ComunaService comunaService;

    @Inject
    private JsfUtils jsfUtils;

    @Getter
    @Setter
    private Long idPersona;

    @Getter
    @Setter
    private Persona persona;

    @Getter
    @Setter
    private Integer idRegion;

    @Getter
    @Setter
    private Integer idProvincia;

    @Getter
    @Setter
    private Integer idComuna;

    @Getter
    @Setter
    private List<SelectItem> regionesSelectItems;

    @Getter
    @Setter
    private List<SelectItem> provinciasSelectItems;

    @Getter
    @Setter
    private List<SelectItem> comunasSelectItems;

    public void onLoad() {
        log.debug("=> entrando a: PersonaBean");
        var optional = this.jsfUtils.getRequestParameterLong("idPersona");
        this.idPersona = optional.isPresent() ? optional.get() : null;

        if (this.idPersona != null) {
            this.persona = this.personaService.traerPorIdConFetchs(this.idPersona);
            this.idComuna = this.persona.getComuna().getIdComuna();
            this.idProvincia = this.persona.getComuna().getProvincia().getIdProvincia();
            this.idRegion = this.persona.getComuna().getProvincia().getRegion().getIdRegion();
        } else {
            this.persona = new Persona();
        }

        cargarListas();
    }

    public void cambiarRegionListener() {
        this.idComuna = null;
        this.idProvincia = null;
        cargarProvincias();
        cargarComunas();
    }

    public void cambiarProvinciaListener() {
        this.idComuna = null;
        cargarComunas();
    }

    public void guardar() {
        var comuna = new Comuna();
        comuna.setIdComuna(this.idComuna);
        this.persona.setComuna(comuna);
        this.personaService.guardar(persona);
        this.jsfUtils.showInfoMessageInGrowl("Se guardo éxitosamente a la persona");
        this.jsfUtils.sendRedirect("/personas");
    }

    /* privados */

    private void cargarListas() {
        cargarRegiones();
        cargarProvincias();
        cargarComunas();
    }

    private void cargarRegiones() {
        this.regionesSelectItems = this.regionService.traerRegiones().stream()
                .map(region -> new SelectItem(region.getIdRegion(), region.getNombre())).collect(Collectors.toList());
    }

    private void cargarProvincias() {
        if (this.idRegion != null) {
            this.provinciasSelectItems = this.provinciaService.traerPorIdRegion(this.idRegion).stream()
                    .map(prov -> new SelectItem(prov.getIdProvincia(), prov.getNombre())).collect(Collectors.toList());
        } else {
            this.provinciasSelectItems = null;
        }
    }

    private void cargarComunas() {
        if (this.idProvincia != null) {
            this.comunasSelectItems = this.comunaService.traerPorIdProvincia(idProvincia).stream()
                    .map(comuna -> new SelectItem(comuna.getIdComuna(), comuna.getNombre()))
                    .collect(Collectors.toList());
        } else {
            this.comunasSelectItems = null;
        }
    }

}
