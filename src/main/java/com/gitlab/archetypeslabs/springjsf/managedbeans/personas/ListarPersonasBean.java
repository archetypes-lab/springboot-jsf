package com.gitlab.archetypeslabs.springjsf.managedbeans.personas;

import java.io.Serializable;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.SessionScope;

import com.gitlab.archetypeslabs.springjsf.cc.personas.BuscadorPersonasComponent;

import lombok.Getter;
import lombok.Setter;

@Component
@SessionScope
public class ListarPersonasBean implements Serializable {

    private static final Logger log = LoggerFactory.getLogger(ListarPersonasBean.class);

    @Getter
    @Setter
    private BuscadorPersonasComponent buscadorComp;

    @PostConstruct
    public void postConstruct() {
        log.debug("se creo una instancia de: ListarPersonasBean");
    }

    public void onLoad() {
        log.debug("Iniciando ListarPersonasBean!");
        this.buscadorComp = new BuscadorPersonasComponent();
        this.buscadorComp.init();
    }

}
