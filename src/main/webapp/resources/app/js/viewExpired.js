$(function () {    
    $(document).on('pfAjaxError', function(xhr, ajaxObj, error) {
    	try {
    		var status = ajaxObj.statusCode().status;
    		var redirectNode = ajaxObj.responseXML.childNodes[0].children[0];
    		var redirectNodeName = redirectNode.tagName;
    		var redirectUrl = redirectNode.getAttribute('url');
    		if(status === 403 && redirectNodeName === 'redirect' && redirectUrl) {
    			window.location.href = redirectUrl; 
    		}
    	}catch(e) {
    		// nada
    	}
    });
});

