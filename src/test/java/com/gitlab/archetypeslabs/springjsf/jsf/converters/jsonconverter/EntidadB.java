package com.gitlab.archetypeslabs.springjsf.jsf.converters.jsonconverter;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class EntidadB {

    private Long id;

    @JsonIgnore
    private EntidadA entidadA;

    @JsonIgnore
    private List<EntidadA> entidadAList;

    public EntidadA getEntidadA() {
        return entidadA;
    }

    public void setEntidadA(EntidadA entidadA) {
        this.entidadA = entidadA;
    }

    public List<EntidadA> getEntidadAList() {
        return entidadAList;
    }

    public void setEntidadAList(List<EntidadA> entidadAList) {
        this.entidadAList = entidadAList;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

}